public class Board{
	private Die dice1;
	private Die dice2;
	private boolean[] closedTiles;
	
	public Board(){
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.closedTiles = new boolean[12];
			for (int i = 0; i < this.closedTiles.length; i++){
				this.closedTiles[i] = false;
			} 
	}
	
	public String toString(){
		int count = 0;
		String array = "";
		
		for(int i = 0; i < this.closedTiles.length; i++){
			count++;
			if(this.closedTiles[i]){
				array = array + "X ";
			} else {
				array = array + count + " ";
			}
		}
		return array; 
	}
	
	public boolean playATurn(){
		dice1.roll();
		dice2.roll();
		System.out.println(dice1);
		System.out.println(dice2);
		int sum = dice1.getPips() + dice2.getPips();
		
		if(this.closedTiles[sum-1]){
			System.out.println("The tile at " + sum + " is already shut!");
			return true;
		} else {
			closedTiles[sum-1] = true;
			System.out.println("Closing tile: " + sum);
			return false;
		}
	}
}